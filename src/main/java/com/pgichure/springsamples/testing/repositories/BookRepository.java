package com.pgichure.springsamples.testing.repositories;


import com.pgichure.springsamples.testing.models.Book;

public interface BookRepository extends JpaRepository<Book, Long>{

    public List<Book> findAllByAuthorId(Long authorId);

}