package com.pgichure.springsamples.testing.repositories;


import com.pgichure.springsamples.testing.models.Author;

public interface AuthorRepository extends JpaRepository<Author, Long>{

}