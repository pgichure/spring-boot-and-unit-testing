package com.pgichure.springsamples.testing.services;

public interface BookServiceI{

    public BookDto getDto(Book book);

    public Book getBook(BookDto bookDto);

}