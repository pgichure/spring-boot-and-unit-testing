package com.pgichure.springsamples.testing.services;


@Service 
@RequiredArgsConstructor
public class AuthorService{

    private final AuthorRepository repository;

    private final BookServiceI bookService;

    private final BookRepository bookRepository;

    @Override
    public AuthorDto save(AuthorDto dto){
        Author author = this.getBook(dto);
        author = repository.save(author);

        return this.getDto(author);
    }

    @Override
    public List<AuthorDto> findAll(){
        List<Author> authors = repository.findAll();

        return authors.stream()
                .map(author -> this.getDto(author))
                .collect(Collectors.toList());

    }

    @Override
    public AuthorDto findById(Long authorId){
        Author author = repository.findById(authorId).orElseThrow(() -> new Exception("Author not found for ID " + authorId));
        return this.getDto(author);
    }

    @Override
    public AuthorDto delete(Long authorId){
        Author author = repository.findById(authorId).orElseThrow(() -> new Exception("Author not found for ID " + authorId));
        AuthorDto dto = this.getDto(author);
        repository.deleteById(authorId);
        return dto;
    }

    @Override
    public AuthorDto addBook(Long authorId, BookDto dto){
        
        Author author = repository.findById(authorId).orElseThrow(() -> new Exception("Author not found for ID " + authorId));
        Book book = bookService.getBook(dto);
        book.setAuthor(author);
        book = bookRepository.save(book);
        return this.getDto(author);
    }

    @Override
    public AuthorDto update(Long authorId, AuthorDto dto){

        Author author = repository.findById(authorId).orElseThrow(() -> new Exception("Author not found for ID " + authorId));

        author = this.getAuthor(dto);
        author = repository.save(author);
        return this.getDto(author);
    }

    private Author getAuthor(AuthorDto dto) {
    	return Author.builder()
                .id(dto.getId())
    			.name(dto.getName())
    			.build();
    }

    
    private AuthorDto getDto(Author author) {
        //add books listing
    	return AuthorDto.builder()
                .id(author.getId())
    			.name(author.getName())
    			.build();
    }

}