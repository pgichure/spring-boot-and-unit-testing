package com.pgichure.springsamples.testing.services;

import com.pgichure.springsamples.testing.dtos;

public interface AuthorServiceI{

    public AuthorDto save(AuthorDto author);

    public List<AuthorDto> findAll();

    public AuthorDto addBook(Long authorId, BookDto book);

    public AuthorDto update(Long authorId, AuthorDto author);

    public AuthorDto findById(Long authorId);

    public AuthorDto delete(Long authorId);

}
