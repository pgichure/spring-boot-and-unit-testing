package com.pgichure.springsamples.testing.services;

public class BookService{

    public BookDto getDto(Book book){
        return BookDto.builder()
    			.name(book.getName())
                .isbn(book.getIsbn())
                .edition(book.getEdition())
                .year(book.getYear())
                .authorId(book.getAuthor() == null ? null : book.getAuthor().getId())
    			.build();
    }

    public Book getBook(BookDto bookDto){
        return Book.builder()
    			.name(bookDto.getName())
                .isbn(bookDto.getIsbn())
                .edition(bookDto.getEdition())
                .year(bookDto.getYear())
                .author()
    			.build();
    }

}