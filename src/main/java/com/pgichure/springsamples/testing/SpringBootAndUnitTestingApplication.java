package com.pgichure.samples.testing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAndUnitTestingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAndUnitTestingApplication.class, args);
	}

}
