package com.pgichure.springsamples.testing.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Entity
@Table(name = "authors")
public class Author implements Serializable{
 
	private static final long serialVersionUID = 1L;

    @Column(name ="name")
	private String name;

}