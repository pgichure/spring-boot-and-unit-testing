package com.pgichure.springsamples.testing.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Entity
@Table(name = "books")
public class Book implements Serializable{
 
	private static final long serialVersionUID = 1L;

    @Column(name ="name")
	private String name;

    @Column(name ="isbn")
	private String isbn;

    @Column(name ="edition")
	private String edition;

    @Column(name ="year")
	private Integer year;

    @ManyToOne(JoinColumn = "author_id")
    private Author author;

}