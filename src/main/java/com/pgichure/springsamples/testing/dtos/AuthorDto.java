package com.pgichure.springsamples.testing.dtos;

import java.io.Serializable;
import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthorDto {

	private String name;

    private List<BookDto> books;

}