package com.pgichure.springsamples.testing.dtos;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BookDto {
 
	private String name;

	private String isbn;

	private String edition;

	private Integer year;

    private Long authorId;

}