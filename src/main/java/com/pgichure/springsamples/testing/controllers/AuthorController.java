package com.pgichure.springsamples.testing.controllers;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pgichure.springsamples.testing.services.AuthorServiceI;
import com.pgichure.springsamples.testing.dtos.AuthorDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;


@RestController
@RequestMapping("/authors")
@RequiredArgsConstructor
@Api(tags = {"Authors Management"}, description = "Operations on authors management")
public class AuthorController{

    private final AuthorServiceI service;

    @PostMapping
    @ApiOperation(value = "Save an author", response = AuthorDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully saved the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public ResponseEntity<AuthorDto> save(@ReponseBody AuthorDto author){
        author = service.save(author);
        return new ResponseEntity(author, HttpStatus.OK);
    }

    @GetMapping
    @ApiOperation(value = "Fetch authors listing", response = List<AuthorDto>.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the records"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<List<AuthorDto>> getAll(){
    	List<AuthorDto> authors = return service.findAll();
        return new ResponseEntity(authors, HttpStatus.OK);
    }

    @GetMapping(value ="{/id}")
    @ApiOperation(value = "Fetch author by Id", response = AuthorDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<AuthorDto> findById(@PathVariable Long id){
    	AuthorDto author = return service.findById(id);
        return new ResponseEntity(author, HttpStatus.OK);
    }   

    @PutMapping(value ="{/id}")
    @ApiOperation(value = "Update author by Id", response = AuthorDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully updated the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<AuthorDto> update(@PathVariable Long id, @ResponseBody AuthorDto author){
    	AuthorDto author = return service.update(id, author);
        return new ResponseEntity(author, HttpStatus.OK);
    }   
    

    @DeleteMapping(value ="{/id}")
    @ApiOperation(value = "Delete author by Id", response = AuthorDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted the record"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The record you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "The server encountered an error")
    })
    public  ResponseEntity<AuthorDto> delete(@PathVariable Long id){
    	AuthorDto author = return service.delete(id);
        return new ResponseEntity(author, HttpStatus.OK);
    }   

}