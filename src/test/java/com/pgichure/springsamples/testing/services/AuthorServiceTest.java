package com.pgichure.springsamples.testing.services;


import com.pgichure.springsamples.testing.services.AuthorServiceI;
import com.pgichure.springsamples.testing.repositories.AuthorRepository;

@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class AuthorServiceTest{

    @InjectMocks
    private AuthorServiceI service;

    @Mock
    private AuthorRepository repository;

    @Test
    void should_save_one_author() {
        // Arrange
        final var authorToSave = Author.builder().name("Paul Gichure").build();
        when(repository.save(any(Author.class))).thenReturn(authorToSave);

        // Act
        final var actual = service.save(new Author());

        // Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(authorToSave);
        verify(repository, times(1)).save(any(Author.class));
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_find_and_return_one_author() {
        // Arrange
        final var expectedAuthor = Author.builder().name("Paul Gichure").build();
        when(repository.findById(anyInt())).thenReturn(Optional.of(expectedAuthor));

        // Act
        final var actual = service.findById(getRandomInt());

        // Assert
        assertThat(actual).usingRecursiveComparison().isEqualTo(expectedAuthor);
        verify(repository, times(1)).findById(anyInt());
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_not_found_a_author_that_doesnt_exists() {
        // Arrange
        when(repository.findById(anyInt())).thenReturn(Optional.empty());

        // Act & Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> service.findById(getRandomInt()));
        verify(repository, times(1)).findById(anyInt());
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_find_and_return_all_authors() {
        // Arrange
        when(repository.findAll()).thenReturn(List.of(new Author(), new Author()));

        // Act & Assert
        assertThat(service.findAll()).hasSize(2);
        verify(repository, times(1)).findAll();
        verifyNoMoreInteractions(repository);
    }

    @Test
    void should_delete_one_author() {
        // Arrange
        doNothing().when(repository).deleteById(anyInt());

        // Act & Assert
        service.deleteOneStudent(getRandomInt());
        verify(repository, times(1)).deleteById(anyInt());
        verifyNoMoreInteractions(repository);
    }

    private int getRandomInt() {
        return new Random().ints(1, 10).findFirst().getAsInt();
    }
}