package com.pgichure.springsamples.testing.repositories;


import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.tutorialspoint.sprintbooth2.SprintBootH2Application;

import com.pgichure.springsamples.testing.models.Author;
import com.pgichure.springsamples.testing.repositories.AuthorRepository;

@ExtendWith(SpringExtension.class)
@Transactional
@SpringBootTest(classes = SprintBootH2Application.class)
public class AuthorRepositoryTest{

   @Autowired
   private AuthorRepository authorRepository;

   @Test
   public void testFindById() {
      Author author = getAuthor();	     
      authorRepository.save(author);
      Author result = authorRepository.findById(author.getId()).get();
      assertEquals(author.getId(), result.getId());	     
   }

   @Test
   public void testFindAll() {
      Author author = getAuthor();
      authorRepository.save(author);
      List<Author> result = new ArrayList<>();
      authorRepository.findAll().forEach(e -> result.add(e));
      assertEquals(result.size(), 1);	     
   }


   @Test
   public void testSave() {
      Author author = getAuthor();
      authorRepository.save(author);
      Author found = authorRepository.findById(author.getId()).get();
      assertEquals(author.getId(), found.getId());	     
   }


   @Test
   public void testDeleteById() {
      Author author = getAuthor();
      authorRepository.save(author);
      authorRepository.deleteById(author.getId());
      List<Author> result = new ArrayList<>();
      authorRepository.findAll().forEach(e -> result.add(e));
      assertEquals(result.size(), 0);
   }
   
   private Author getAuthor() {
    	return Author.builder()
                .id(1L)
    			.name("Paul Gichure")
    			.build();
    }

}