package com.pgichure.springsamples.testing.controllers;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.pgichure.springsamples.testing.services.AuthorServiceI;
import com.pgichure.springsamples.testing.dtos.AuthorDto;

@Slf4j
@ExtendWith(SpringExtension.class)
@WebMvcTest(value = AuthorController.class)
@WithMockUser
public class AuthorControllerTest{

    @Autowired
	private MockMvc mockMvc;

	@MockBean
	private AuthorServiceI authorService;


    AuthorDto mockAuthor = AuthorDto.builder()
                .id(1L)
    			.name("Paul Gichure")
    			.build();

    String exampleAuthorJson = "{\"name\":\"Paul Gichure\",\"id\":1}";

    @Test
	public void retrieveDetailsForAuthor() throws Exception {

		Mockito.when(authorService.retrieveCourse(Mockito.anyString(),
						Mockito.anyString())).thenReturn(mockAuthor);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/authors/1").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		log.info(result.getResponse());

		String expected = "{\"id\":\"1\",\"name\":\"Paul Gichure\"}";

		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}

    @Test
	public void createAuthor() throws Exception {
		Course mockAuthor = AuthorDto.builder()
                .id(1L)
    			.name("Paul Gichure")
    			.build();

		Mockito.when(authorService.save(Mockito.anyString(), Mockito.any(Author.class))).thenReturn(mockAuthor);
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/authors")
				.accept(MediaType.APPLICATION_JSON).content(exampleAuthorJson)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.CREATED.value(), response.getStatus());

		assertEquals("http://localhost:8181/authors/1", response.getHeader(HttpHeaders.LOCATION));

	}


}