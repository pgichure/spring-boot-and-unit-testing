# Spring Boot And Unit Testing
This project is an example of unit testing in Spring Boot for controller, service and repository layers.

## Running the Project

```
cd working_directory
git clone https://gitlab.com/pgichure/spring-boot-and-unit-testing.git
cd spring-boot-and-unit-testing
mvn test
```
To see the report, go to target/site/jacoco/index.html and open the file to view the coverage